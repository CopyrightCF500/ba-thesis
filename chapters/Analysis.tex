% !TEX root = ../main.tex
\chapter{Analysis of the Lane Follower}
\label{chapter:Analysis}
The Lane Follower was initially thought out and implemented within the \gls{ADTF} environment in the summer semester of 2017 by Florian Bassler, a student at the time. The execution in the \gls{ADTF} context at that time was very accurate and the \gls{RC}-Car reliable followed the lane on the test track at a constant speed and without large steering fluctuations. \\
When the switch to the \gls{ROS} system was made in 2019, the lane follower was one of the first modules ported from the \gls{ADTF} project to the new \gls{ROS} project and adapted to the new architecture and \gls{ROS} interfaces and principles. \\ 
The Lane Follower algorithm is divided into several sub-processes and classes, and has a very complex flow with an own communication system. \\
The following chapter analyzes and describes the processes within the lane follower and the integration into the \gls{ROS} system with related and used \gls{ROS} nodes. In addition, problems and difficulties are worked out, which arose mainly through the porting.

\section{Workflow and Related ROS Packages}
In order to understand the complex process flow of the lane follower, the focus of the following chapter is to visualize the data flow in the \gls{ROS} system for the Lane Follower and to bring it closer. \\
In addition to the general overview of the connections of the various \gls{ROS} nodes and the topics used, some related \gls{ROS} nodes are examined in more detail. This clarifies the already integrated process steps and further useful interfaces for the optimization approaches are analyzed. \\

\noindent This section first describes the general workflow of the lane follower, focusing on the data flow in the overall system, the information transfer and processing within the lane follower, and looks at the package structure using a UML class diagram.

\subsection{Data Flow of Lane Follower}

Figure \ref{fig:lane_follower_rqt} shows a representation of the nodes used, that are essential for the execution of the lane follower. Furthermore it displays the Publications and Subscriptions to the specific topics and their corresponding message types. \\
The figure was created independently, in which \gls{ROS} nodes were represented in ellipse from and \gls{ROS} topics in horizontal rectangles. The corresponding types of sent messages, are arranged above or below the topic respectively. \\
The arrows visualize input and output of a node, is based on the Publisher-Subscriber pattern and uses \gls{ROS} Topics described in section 2.2.2. An outgoing arrow describes the publishing of processed data, while an incoming arrow shows the use of some filtered information. \\
The visualization starts on the far left with the image recording and ends on the right in the car controller, where the steering angle is passed on the actuators. \\

\begin{figure}[ht]
	\centering
	\includegraphics[width=13cm]{images/lane_follower_ros_pipeline.png}
	\caption{Workflow of the Lane Follower in the \gls{ROS} system}
	\label{fig:lane_follower_rqt}
\end{figure}

\noindent For the recognition of the road lines and the corresponding following of the lane, the camera image is first recorded and prepared accordingly. This happens within the Pylon and Fisheye Undistortion node, where the pylon node is the official pylon \gls{ROS} driver for Basler GigE Vision and USB vision cameras, which includes the driver itself and message and service definitions for interacting with the camera driver\cite{ROS_PYLON}. The unprocessed image message stream is then captured by the fisheye undistortion node configured and distorted for further processing. \\

\noindent The rectified and configured camera image is then passed on to the lane follower. The lane follower need the image to recognize the line segments and calculate the center line of the lane to compute a steering angle. A detailed description of the individual process steps within the lane follower can be found in section 3.3. \\
Accordingly, the lane follower node has only one input value and one output, which are relevant for the main flow. As input the calibrated camera image as sensor message image, and the calculated steering angle in decimal from as float value. The activation and deactivation through the remote controller is described in more detail below. \\

\noindent The computed steering angle for every image in the video stream is then sent to a modified \gls{ROS} float multiplexer. This node can receive multiple steering angles from different node modules in the system and forward the one with the highest priority. \\
Currently, there is no PID controller that regulates the target and actual value of the steering angle, which is why it is sent directly to the car controller. The car controller sends the received steering angle to the arduino boards, which passes it directly to the servo motor of the wheels.


\subsection{Speed and Steering Angle Multiplexer}
A multiplexer, also known as a data selector, is a device or entity that selects between several digital input signals and forwards the selected input to a single output line\cite{WIKI_MUX}. With the currently used design architecture, there are multiple \gls{ROS} nodes that computing a desired steering angle and velocity to drive along the test track. The final instance, the car controller, needs to know which steering angle and speed finally should be sent to the actuators. To integrate this priority-based decision functionality, the float Multiplexer was created. This multiplexer is specially designed for float messages and is divided into a speed and a steering angle multiplexer, which are adaptions of the \gls{ROS} twist multiplexer\cite{ROS_MUX}. \\

\begin{figure}[ht]
	\centering
	\includegraphics[width=13cm]{images/float_multiplexer.png}
	\caption{Workflow of Velocity and Steering Angle through Float Mux Package}
\end{figure}

\noindent Figure 3.2 shows the computation steps and transfer of the steering angle and velocity through the float multiplexer until it reaches the car controller and so the actuators. The information transfer from the velocities of the different nodes is marked with blue arrows, while the flow of steering angles is shown with orange arrows. \\

\noindent There are three modules that create and calculates a desired steering angle including the lane follower, the ps4 node and the trajectory follower. The ps4 node enables the possibility to control the vehicle via playstation 4 controller and calculates the desired steering angle from the joystick. Currently the trajectory also sends his steering angle to the multiplexer, but will discussed and described in more detail below. \\
As can be seen in figure 3.3, another node sends a steering angle, the steering angle controller. The steering angle controller is supposed to implement a PID controller, which however is not yet ready and operational. \\

\noindent The speed multiplexer receives four desired velocities of different nodes, selecting one and transmit it to the speed controller. In addition to the trajectory follower and the ps4 node, which already transmitting a steering angle, the emergency brake and the stvo node calculates desired speed and send it to the multiplexer. The emergency brake module takes information of the attached lidar system and sends a speed of zero, when something is detected in front of the \gls{RC}-Car. The stvo package includes the traffic sign detection and is described in subsection 3.1.5. \\
Currently, the speed controller only calculates the percentage speed for the motor based on the physical speed. In the future, as in the steering angle controller, a PID will be implemented.

\noindent To tell the specific multiplexers which topics they listen to and which one they should prioritize, a configuration yaml file was created. The table below visualizes all corresponding topics and priorities.

\begin{figure}[ht]
	\centering
	\includegraphics[width=12cm]{images/float_mux_topics.png}
	\caption{Configuration file of both float multiplexers}
\end{figure}

\subsection{Remote Controlling}
The VDI ADC sets various static and dynamic tasks that the autonomous vehicle should master. These mainly include driving as quickly as possible along an unknown test track, detecting possible collisions and parking in a parking space. \\
But communication via the internet with other road users and remote control of the vehicle in unsolvable situations are also becoming increasingly important, which is why a further requirement of the Challenge is that all disciplines has to initiated by a wi-fi signal. \\

\begin{figure}[ht]
	\centering
	\includegraphics[width=14cm]{images/remote_controller_web.png}
	\caption{The Web-based Remote Controller}
\end{figure}

\noindent For this feature, the remote controller was created. The remote controller is a web-based component that gives the user the option of sending commands which should be executed to the car, which can be seen in figure 3.4. \\
Currently, five node functionalities can be started on the web page, which is hosted on the car itself. The lane follower, emergency braking, speed control, parking and the parcours controller. The parcours controller starts the lane follower and sends another output format. By selecting one module and defining a duration and a distance within the speed controller, the \gls{RC}-Car executes the corresponding function and terminates it as soon as the entered time has elapsed.
By starting the lane follower, currently the stvo package will be started as well to send a static velocity. For the parking module, there is one input field for selecting the parking slot, and another one for the pullout direction. \\

\begin{figure}[ht]
	\centering
	\includegraphics[width=12cm]{images/remote_controller.png}
	\caption{Relations of the Remote Controller}
\end{figure}

\noindent Figure 3.5 visualizes the controllable nodes and the communication streams from the remote controller through topics. In all cases the nodes are listening to their corresponding topic of the remote controller, which type is a standard bool message to activate or deactivate the module. Additionally, the remote controller subscribes two topics, which informs it when the parking process is done and after the parking pullout execution finished. The stvo package is only started through the lane follower, the sprint module is currently not integrated in the workflow system and the parcours controller functionality will be transferred into the lane follower and accordingly removed.

\subsection{Trajectory Multiplexer and Clothoids}
The separate handling of speed and steering angle as described in subsection 3.1.2 not only causes message overload and confusion, but also does not correspond to the actual path planning in an autonomous vehicle mentioned in section 2.1.4. \\
For this reason, these two pieces of information were bundled into trajectories in the winter semester 2020 and the trajectory multiplexer and trajectory follower were created, which will replace the previous used float multiplexer. \\

\noindent The trajectory multiplexer receives trajectories from the packages that plan the cars trajectories. The topics it subscribes to and their priorities are declared in a configuration file, summarized in table 3.7. It then forwards the trajectory with the highest priority to the trajectory follower. Trajectories with the same priority are assumed to come from the same publisher and therefore will always be forwarded. \\
To give way to other packages with lower priority, a trajectory with zero segments needs to be published or the follower has to be informed that the last trajectory is completed.

\begin{figure}[ht]
	\centering
	\includegraphics[width=13cm]{images/trajectory_pipeline.png}
	\caption{Workflow of steering angle and velocity through trajectories}
\end{figure}

\noindent The Trajectory Follower receives the selected trajectory, filters the steering angle it contains and sends it directly to the car controller, just like the steering angle multiplexer does. The velocity information is send to the speed multiplexer and gets converted to the percentage speed for the electronic speed control (ESC)]. \\
As can be seen in figure 3.6, trajectories are sent using a newly created message type, clothoids. Clothoids, also known as Euler Spiral or Cornu Spiral, are curves whose curvature increases proportionally with their length\cite{WIKI_CLO}, and can be seen in figure 3.7 on the left hand side. \\

\noindent The following formula applies:

\begin{equation}
	R = \frac{A^{2}}{L} , L > 0
	\label{eq:clothoid}
\end{equation}

\noindent R is the radius of the circle at that exact location, L is the arc length, and A is a constant parameter. However, we represent these curves with the help of three parameters: The length, the curvature and the change in curvature. As shown in the middle visualization of figure 3.7, the trajectories are composed of several clothoid segments. For the first segment, a length, an initial curvature and a curvature change are transferred. For all following segments the curvature at the end of the previous segment is used as initial curvature. This avoids bumping in the trajectory. \\
The graphic on the right in figure 3.7 shows the defined custom clothoid message. Additionally to the three parameters described before, the message contains a unique id, the number of combined segments and also the velocity information for the segments.

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[b]{0.3\textwidth}
		\centering
		\includegraphics[width=\textwidth]{images/clothoid.png}
		\caption{The Euler Spiral}
	\end{subfigure}
	\hfil
	\begin{subfigure}[b]{0.3\textwidth}
		\centering
		\includegraphics[width=\textwidth]{images/clothoid_segments.png}
		\label{fig:generaloffset}
		\caption{Composition several clothoid segments}
	\end{subfigure}
	\hfil
	\begin{subfigure}[b]{0.3\textwidth}
		\centering
		\includegraphics[width=\textwidth]{images/clothoid_msg.png}
		\label{fig:generaloffset}
		\caption{Clothoid Message Type}
	\end{subfigure}
\end{figure}


\subsection{Line Segment Detection Node}
At the beginning of the summer semester 2021, it has already been recognized that so far two modules require line recognition and more will be added in the future. This performance consuming task is accomplished using the opencv library and is performed so far in the parking and lane follower package respectively. \\
For this reason, after joint consideration and agreement, a separate \gls{ROS} package was implemented for the line segment detection (LSD), which was then also connected to the parking module. \\

\noindent The LSD package provides the ability to detect line segments in the current camera frame based on a region of interest, provided as a \gls{ROS} topic by the user nodes. The detected lines then get published. For communication with other nodes the package subscribes and publishes various topics, which are visualized in figure 3.7. \\
The images are provided by the fisheye undistortion package, while the LSD waits for a defined region of interest where to detect the lines. After computation, the detected lines get published in a custom message type lineSegment. It consists of an array of lines, which are defined as four float variables for the edges. Another information published is the current camera frame with the currently detected lines as an overlay. It can be used for debugging or further processing.

\begin{figure}[ht]
	\centering
	\includegraphics[width=13cm]{images/lsd_pipeline.png}
	\caption{Pipeline of the Line Segment Detection}
\end{figure}

\noindent Initially started, the node does nothing when receiving an image, only if a \gls{ROI} is set, the node starts processing. It is not necessary to publish \gls{ROI}'s multiple times, because the LSD node remembers it once it has been sent. \\
As soon as both, a camera frame and a \gls{ROI} is present, the node starts to act, starting with the converting the image to a binary image. The image is then masked using the \gls{ROI} for the following execution of LSD algorithm. \\
The LSD package is an important component for the subsequent optimization of the lane follower.

\subsection{Traffic Sign Detection}
Street sign recognition plays an important role for different modules of the system, where a specific action needs to be performed at different locations. There are various traffic signs on the test track, but in the current state of the software, only the recognized parking sign is used to execute the parking algorithm. \\

\begin{figure}[ht]
	\centering
	\includegraphics[width=8cm]{images/traffic_signs.png}
	\caption{Traffic Signs with corresponding fiducial markers}
\end{figure}

\noindent The detection of the traffic signs does not use classic object detection, instead the official \gls{ROS} package Aruco Detect\cite{ROS_ARUCO} is used, to detect fiducial markers that are combined with the traffic signs, shown in figure 3.7. \\
The aruco detect node takes the undistorted image of the fisheye undistortion package, detects all the fiducial markers in the frame and sends it to the fiducial vertices topic. The stvo module takes the information in the fiducials array, maps the fiducial Ids to the traffic signs and publishes them to the /stvo/traffic\_signs topic, which has a custom message type holding an array of all detected signs. \\
This information is currently only used by the parking package to determine when to start the parking process. Additionally the stvo package sends continuously a constant velocity to the speed multiplexer the moment the node is started. If the \gls{RC}-Car should drive in a different speed, the static variable in the stvo package must be changed. \\
Figure 3.9 visualizes the pipeline of the stvo package with all the relevant nodes.

\begin{figure}[ht]
	\centering
	\includegraphics[width=13cm]{images/traffic_signs_pipeline.png}
	\caption{Pipeline of the Traffic Sign Detection}
\end{figure}


\section{Package Structure}
The following section analyses and describes the actual \gls{ROS} Lane Follower package with all his classes, relations and workflow. First, the design architecture and home-made communication systems between the different modules are described, followed by a visualization and briefly introduction of the main Lane Follower sequence. In the last sub-section, all the modules, used classes and header files are described in more detail and are underlined by screenshots and tables.

\subsection{Design Architecture}
The Lane Follower consists of many different modules and classes, which are either directly necessary for the algorithm or implement visualizations, performance measurements and other utility modules, which have developed mainly through the previous \gls{ADTF} framework. \\
The exchange of information between these different modules is enabled by a dedicated communication architecture. For this purpose, a sender module and a receiver module were written as an interface, which are implemented and applied by the respective modules. For an overview of the interface and the virtual methods, corresponding UML class diagrams are presented in figure 3.13.

\begin{figure}[ht]
	\centering
	\includegraphics[width=13cm]{images/receiver_sender_interfaces.png}
	\caption{Interfaces for communication between modules}
\end{figure}

\noindent The receiver interfaces defines how an object can serve as a module that receives data that has been processed before. A class that implements this interface is capable to process calls to process(T...) with the specified number of parameters and a return value R. \\
The sender interface defines how an object can serve as a module that can send data that it has processed. A class that implements this interfaces is capable to call R process(T...) of modules that implement the receiver module. This is used to create a chain of connected modules that receive information from another module, process it, and send it to another module. \\

\noindent On the top level is the Lane Follower class, which is the interface to the \gls{ROS} system. Here, the input and output, i.e. the necessary subscribers and publishers are created in order to receive the necessary information and to forward the result. \\
This class was created during porting to adapt the existing algorithm to the new \gls{ROS} framework and was rewritten and refactored within this thesis. \\
The class has an instance of the Road Follower Planner (RFP), which implements the actual flow of the algorithm. Within the Constructor the RFP sets up the complete image processing pipeline by chaining the modules to build the lane detection pipeline. This linking of modules is performed via the registerModule method and can be seen in code section 3.1. \\

\begin{listing}
	%the language syntax can be declared here.
	\begin{minted}[obeytabs=true,tabsize=2]{c++} 
		cFilterRoadFollowPlanner::cFilterRoadFollowPlanner() :
		m_moduleTransmitCurvature(this)
		{
			// Set up the complete image processing pipeline used:
			// Chain the modules to build the lane detection pipeline
			m_moduleDetectLineSegments.registerModule
			(m_moduleCameraToCarSpace);
			m_moduleCameraToCarSpace.registerModule
			(m_moduleProjectLineSegments);
			m_moduleProjectLineSegments.registerModule
			(m_moduleDetectLaneCenter);
			m_moduleDetectLaneCenter.registerModule
			(m_moduleLaneToCurvature);
			m_moduleLaneToCurvature.registerModule
			(m_moduleTransmitCurvature);
			
			Configure();
		}
	\end{minted}
	
	\caption{Callback function of detected Traffic Signs}
\end{listing}

\noindent It then configures all the initialized modules with the needed parameters like the camera position for the coordinates transformation or the sampling rays, which are used to detect the lane center. \\
The algorithm starts by calling the handleDetectedLines function using the input and output image as parameters. The inherited process method of the LSD detection module gets called and invokes the next modules process method after finishing the computation. \\
Figure 3.13 shows the complexity and relations of all modules within the Lane Follower package.

\begin{sidewaysfigure}[ht]
	\centering 
	\includegraphics[width=21cm]{images/package_uml.png}
	\caption{Package Structure of the Lane Follower}
	\label{fig:package_uml}
\end{sidewaysfigure}

\subsection{Algorithm Pipeline}
The computation of the lane center and the corresponding desired steering angle is split up into four main steps, to reduce the complexity of the algorithm and separate different functionalities into isolated modules. \\
In the first step, the openCV LSD algorithm detects line segments in the captured image, which are stored in pixel coordinates. The next step is the transformation of the 2D pixel coordinates into 3D vehicle coordinates. This step is necessary for the subsequent projection of the lines. The projection mapping the lines onto the ground depending on the defined camera angle. As can be seen in the third image c of figure 3.12, the road is now viewed from a bird's-eye view, whereby the road has the same width at every point.\\
The view from above then enables the determination of the center line in the last step, which is then used to create a steering angle for the current camera image. \\
Figure 3.12 visualizes these steps and were created by Florian Bassler in 2017.

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[b]{0.4\textwidth}
		\centering
		\includegraphics[width=\textwidth]{images/step_1.png}
		\caption{Line Segment Detection}
	\end{subfigure}
	\hfil
	\begin{subfigure}[b]{0.4\textwidth}
		\centering
		\includegraphics[width=\textwidth]{images/step_2.png}
		\label{fig:generaloffset}
		\caption{Pixel coordination to vehicle coordination}
	\end{subfigure}
\end{figure}

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[b]{0.4\textwidth}
		\centering
		\includegraphics[width=\textwidth]{images/step_3.png}
		\label{fig:generaloffset}
		\caption{Line Projection}
	\end{subfigure}
	\hfil
	\begin{subfigure}[b]{0.4\textwidth}
		\centering
		\includegraphics[width=\textwidth]{images/step_4.png}
		\caption{Determination of lane center}
	\end{subfigure}	
	\caption{Main process steps for computing the desired steering angle}
\end{figure}

\subsection{Modules and Header Description}
The modules folder includes the needed header files, dependencies and structs like the receiver and sender modules or the \gls{SAM}/\gls{AADC} structs, but also 14 folders with its modules and classes. These modules are divided into several utility functionalities and 6 algorithm-relevant modules with eight classes.
 which can also be seen in figure \ref{fig:package_uml} on the upper level of the include/modules folder and are listed below: \\

\vspace{-5mm}
\setlist{noitemsep} 
\begin{itemize}
	\item \textbf{lsd\_detection}
	\vspace{-2mm}
	\begin{itemize}
		\item cModuleLsdDetector.cpp
	\end{itemize}
	\item \textbf{convert\_camera\_linesegments}
	\vspace{-2mm}
	\begin{itemize}
		\item cModuleCameraToCarSpace.cpp
	\end{itemize}
	\item \textbf{project\_line\_segments}
	\vspace{-2mm}
	\begin{itemize}
		\item cModuleProjectLineSegments.cpp
	\end{itemize}
	\item \textbf{detect\_lane\_center}
	\vspace{-2mm}
	\begin{itemize}
		\item cModuleDetectLaneCenter.cpp
		\item cModuleSegmentIntersector.cpp
	\end{itemize}
	\item \textbf{lane\_to\_curvature}
	\vspace{-2mm}
	\begin{itemize}
		\item cModuleLaneToCurvature.cpp
	\end{itemize}
	\item \textbf{transmitting}
	\vspace{-2mm}
	\begin{itemize}
		\item cModuleTransmitCurvature.cpp
		\item cModuleReceiveLane.cpp
	\end{itemize}
\end{itemize}

\noindent At the beginning, the LSD detector sets the corresponding \gls{ROI} in which the relevant lines for the track detection are located. In addition, the reduced image is converted to a grayscale format, which is required by the following algorithm. For the actual line detection the Line Segment Detector of the OpenCV library\cite{OpenCV} is used with the detect() method, which need the grayscale image and a vector Vec4f for the detected lines. \\
Figure 3.15 visualizes the detected lines within the \gls{ROI} and highlights them red. \\

\begin{figure}[H]
	\centering
	\includegraphics[width=10cm]{images/detected_lines.png}
	\caption{Detected lines within the \gls{ROI}}
\end{figure}

\noindent This vector of detected line segments is then forwarded to the CameraToCarSpace module which transforms the coordinate system from the 2D input image to directional vectors in the coordinate system of the car. For this purpose, the start and end points of a recognized line are first separated and individually converted into 3D coordinates, after which they are reassembled and stored in a cv::Vec6f vector. This transformation can be seen in figure 3.12 from picture a) to picture b). \\

\noindent After transforming the 2D camera image with the detected lines into a 3D view from the car's perspective, the ProjectLineSegments module creates a 2D view of the road layout from a bird's eye view. All lines in the vector are passed individually to a separate function, which then splits the start and end points. The individual points are then passed to another function, which performs the projection and return the calculated angle of the points. \\
The black debug rectangle within the captured image can be seen in figure 3.16, where the projected lines are highlighted in green. \\

\begin{figure}[H]
	\centering
	\includegraphics[width=10cm]{images/project_lines.png}
	\caption{Debug Visualization of projected lines}
\end{figure}

\noindent The projected lines serve as input for the actual computation of the centerline that the vehicle should follow. The algorithm is implemented using two main classes, where the LineSegmentIntersector class is an utility class that provides methods to find intersecting 2D line segments. It manages an intern set of line segments and can return a list of all lines that intersect with a given one. It also is used to determine the intersection of two infinite lines defined by two line segments, to check if two lines even intersect or for testing if a given point is to the right of a given line segment. \\
These utility functions are especially used by the DetectLaneCenter module, which gets activated by receiving the detected and projected lines.

\section{Identified Issues}
The Lane Follower was originally created within the \gls{ADTF} framework and is therefore based on design structures that are no longer available in the \gls{ROS} system. During the porting to \gls{ROS}, the algorithm with all classes was copied, as described in (section 3.2), and a new class was created at the top level, which enables communication with the other \gls{ROS} nodes. \\
This has enabled the detection and following of the road in the new system, but has also left open many improvements, which are described in more detail below.

\subsection{Unstable Driving Behavior}
The most obvious problems can be seen by observing the vehicle as it drives the test track. When following the lane, the car does not drive continuously in the middle of the lane and always gets a little out of the lane, so that sudden reactions occur. This can become a hazard, especially with possible incoming traffic. It also increases the chances of the car leaving the lane completely and then not finding its way back onto the road and braking off. \\
The reason for this behavior can be that the lane follower only optimizes the position according to the detected center line at the current time and does not know in a predictive way when it has to turn a certain steering angle at which time. \\
In addition, it can be seen that the \gls{RC}-Car does not travel at a continuous speed and repeatedly brakes and accelerates. Again, this behavior is likely due to not using trajectories for computing steering angle and velocity in the future. However, too much change in steering angle or mistakenly detecting objects on the track can also lead to spontaneous throttling of the speed.

\subsection{Redundant and Deprecated Functionalities}
A central problem, which was worked out during the analysis, is the redundant detection of line segments. In the current software version, this is not only carried out in the Lane Follower, but the lines of the parking space are also recognized when parking in and out in order to create a corresponding trajectory. \\
Line detection is done in both cases using the openCV library, which is computationally intensive and uses a lot of memory and power. Therefore, code that is used in multiple \gls{ROS} packages should be separated out and only used when needed. \\
In addition, the Lane Follower algorithm uses several header files with defined C++ structs, which were copied from the \gls{ADTF} project into the package. These headers are also used by other \gls{ROS} packages and were placed in a root folder to access them from anywhere. The Lane Follower does not point to this folder but stores the files locally. \\

\noindent When porting the Lane Follower, the complete folder was copied into the \gls{ROS} project without checking if the respective modules are needed or used. As a result, many classes and functionalities have been ported that were needed and used in the old \gls{ADTF} project, but are now obsolete in the \gls{ROS} project. Several of the modules described above use deprecated dependencies licensed from Audi, which must be removed to make the project publicly available. \\

\noindent In addition, the current software architecture uses a total of three multiplexers to select which calculated control of the vehicle should be sent to the actuators. These include the Steering Angle Multiplexer, which receives different steering angles and forwards a prioritized one, the Speed Multiplexer, which does the same with calculated velocities, and the Trajectory Multiplexer, which bundles both of theses information and sends the to the car controller for a certain time interval using clothoids. This realization is not only redundant, but also greatly complicates the general flow and makes it difficult to understand the software. \\

\subsection{Further Optimization Options}
Besides the identified problems, there are further optimization possibilities that would improve the lane follower and the general behavior of the \gls{RC}-Car. \\
Creating a trajectory to plan more into the future rather than reacting to the current situation would increase stability and accuracy when following the road. In order to provide the necessary speed, the Lane Follower should also use the sign recognition to brake or accelerate accordingly on a curve or straight section of the track. \\
However, during the summer semester of 2021, it was discovered that the Trajectory Follower does not always accurately follow the passed trajectory and exhibits non-deterministic behavior. This may be due the missing PID controllers or a misbehaving implementation of the corresponding \gls{ROS} package. This topic should therefore be analyzed and examined in more detail in general, which would, however, exceed the scope of this thesis. \\
If the Lane Follower creates a trajectory, the functionality of the Parcours Controller is redundant and can be removed. This would benefit the clarity of the overall project, as currently many simple functions are stored in their own \gls{ROS} packages. \\
To minimize the computational effort in detecting the lines and to better exclude unnecessary areas of the image, a dynamic ROI can also be considered, which is adjusted depending on the current steering angle. \\