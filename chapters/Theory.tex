% !TEX root = ../main.tex
% Theoretical Part and Preliminaries
\chapter{Background Theory}
\label{chapter:Theory}
To place the lane follower correctly in the overall technical context, the
following chapter provides theoretical foundations of
advanced-driver-assistance-systems (\gls{ADAS}), an insight into the development with the robotic platform \gls{ROS}, and an introduction to the project study.

\section{ADAS in the real world}
The advanced driver assistance systems cover a wide range of active and
passive systems that are designed to assist the driver by providing safety,
comfort, efficiency while driving and also improves the driver, passenger, and
pedestrian security and safety.\cite{ADAS20} \\
Not only car manufacturers like BMW, Daimler or Tesla are publishing cars with a
high level of autonomy, but also companies like Nvidia, Apple or Google are
hardly developing advanced driver-assistant systems for the private and
commercial use. \\
The development of an autonomous vehicle is highly complex and technically not
always possible to transfer exactly to an \gls{RC}-Car. Nevertheless, the general
theory and design concepts serve as a guideline for the development of the
\gls{RC}-Car in the project study.

\subsection{Autonomy Levels}
The industry has established a five-stage system that classifies the individual
levels of autonomous driving. Figure 1 shows a visual representation of the
different autonomy levels. \\

\begin{figure}[ht]
	\centering
	\includegraphics[width=15cm]{images/j3016-levels.jpg}
	\caption{Autonomy levels according to J3016 SAE Standard\cite{SAE19}}
\end{figure}

\noindent There is no automation at autonomy level 0. The driver permanently
performs all maneuvers on his own without any assistance. Only supportive
warning or intervention systems are used. \\
With autonomy level 1, driver assistance, the car can take over longitudinal OR
lateral guidance, whereas with partial automation, level 2, the vehicle can take
over both assistant systems simultaneously. \\
Within the first three levels of autonomy, the human has always to monitor the
driving environment, must be able to take the controls at any time and is
responsible for every scenario. In the next stages, the automated driving system
takes over monitoring the driving environment. \\
From autonomy level 3, we talk about highly automated driving, and in limited
situations the driver no longer has to monitor the assistance system. However,
the driver must be able to take over the wheel at any time. \\
Level 4 is a driving mode-specific performance by an automated driving system of
all aspects of the dynamic driving task, that does not require the human driver
to respond when prompted to intervene. \\
The difference to level 5 is the full automation, where the driving system
takes control at all times and under all road and environmental conditions and
the driver does not have to monitor at all time and also no longer bears any
responsibility.

\subsection{Sensing, Perception, Decision}
The workflow of an autonomous vehicle can be divided into three main processes.
Sensing, perception and decision. Figure 2.2 demonstrates the \gls{ADAS} architecture
and implementation of these main steps at BMW AG. \\

\noindent Different techniques of sensors are used for sensing. BMW AG relies
on lidar, radar and cameras, which are all mounted at different positions on the
vehicle. The use of sensors differs from company to company, so that Elon Musk
with Tesla, for example, limits himself exclusively to cameras. The
sensors are used to record information from the environment so that it can
be processed in the next step. \\

\noindent The acquired data is then analyzed and processed during the perception
phase. The first step is the fusion of the sensors and data, where all the
different sensors are combined together, to get the best accuracy of the
detected environment. The localization and orientation of the vehicle and the
detection of static and dynamic objects in the environment are represented in
different forms during environment modeling and prepared for further processing.
\\

\noindent The last categorization, the decision, then includes the actual driver
assistance system functions, like the path-, maneuver- or trajectory-planning.
The results of the decisions are then transmitted directly to the physical
vehicle control system and to the human-machine-interface (HMI), allowing the
driver to monitor the system or intervene in the process.

\begin{figure}[ht]
	\centering
	\includegraphics[width=15cm]{images/ADAS_Vehicle_Functional_Architecture.png}
	\caption{\gls{ADAS} architecture of BMW AG\cite{BMW15}}
\end{figure}

\subsection{Lane Following and Related Work}
There are three primary types of systems that helps prevent drivers from drifting around on the road, which have developed and improved in the course of \gls{ADAS} development.
The lane-departure warning (\gls{LDW}), lane-keeping assistance (\gls{LKA}), and lane-centering assistance (\gls{LCA}). \\
\gls{LDW} is the most basic type of lane monitoring technology and alerts the driver when the vehicle is drifting out of its lane by triggering a sound signal or by a haptic warning, such as vibrations through the steering wheel. Since this is a passive system, it does not actively intervene to prevent lane departure. \\
\gls{LKA} systems expands the \gls{LDW} with active prevention.Rather than just alerting the driver, \gls{LKA} semi-autonomously makes correction by gently employing automatic steering and/or braking on the opposite side of the car in order to bring the vehicle back toward the center of the lane. \\
\gls{LCA} is currently the highest level of lane following. This system proactively keeps the vehicle centered within the lane it is traveling. The automatic steering constantly adjusts to the road markings displayed by the front camera.\cite{LCA17} \\

\noindent There are 4 common steps of successful lane detection and following algorithm implementations: image pre-processing, feature extraction, feature validation and trajectory calculation.\\
Images taken directly from the camera usually require certain preparations to make information extraction easier. This includes for example the creation of a Region of Interest (\gls{ROI}) or a greyscale or binary transformation of the image. \\
In feature extraction, the necessary information is gathered from the image, which typically resemble lane markings. Therefore the Canny Edge Detection or the Hough Transform can be used.\cite{KAR16} \\
Feature validation or fitting is the approximation of the extracted features to a smooth path using geometric models. The generated path is later used for various decision-making processes, typically for trajectory or heading calculations. \\
The last step is then the actual trajectory calculation, where the vehicle receives the coordinates for the desired heading. The trajectory is mostly derived on a 2D plane and has to be transformed to real world 3D coordinates. \\

\noindent There are multiple approaches to detect the center lane to keep the vehicle in the lane. \\
In one approach by students from Darmstadt University of Appleid Sciences, the camera is oriented so that the lane boundary can be detected at the height of the wheels. This allows, as shown in figure a) of figure 2.3, to calculate an error deviation how far the car is from the center line and how much the car has to countersteer to the left or to the right. \\

\noindent In another method, the camera is mounted on the front of the car with a view of the road ahead. In this view, the road boundaries are inclined inwards and eventually converge to a common vanishing point at the horizon line. The centerline of the vehicle must now intersect with the horizontal position of the vanishing point so that it can adjust its position to the center of the lane. This approach by calculating a vanishing point is illustrated in figure 2.3 with image b). \\

\noindent As can be seen, there are different approaches to keep the vehicle on the centerline of the road and sending appropriate steering commands to the actuators. Some are more oriented to the course of the road and are more predictive, others react to the current position and have a higher accuracy. \\
The algorithm developed at the Munich University of Applied Sciences has another different approach, which will be described in more detail later in this thesis. \\

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[b]{0.4\textwidth}
		\centering
		\includegraphics[width=\textwidth]{images/offset_of_center_lane.PNG}
		\caption{Approach with downward tilted camera\cite{FUN16}}
	\end{subfigure}
	\hfil
	\begin{subfigure}[b]{0.4\textwidth}
		\centering
		\includegraphics[width=\textwidth]{images/vanishing_point.PNG}
		\label{fig:generaloffset}
		\caption{Approach with vanishing point\cite{KAR16}}
	\end{subfigure}
\caption{Different Approaches for Lane Centering}
\end{figure}

\section{Robot Operating System}
The Robot Operating System (\gls{ROS}) is a framework for writing robot software,
whereas it is not an operating system in the traditional sense of process
management and scheduling, but rather providing a structured communications
layer above the host operating system. \cite{Quig09}
After the Audi Autonomous Driving Cup (\gls{AADC}) is no longer organized, the previously used Automotive Data and Time-Triggered Framework (\gls{ADTF}) has been replaced by \gls{ROS} and the architecture
and foundation of the software will be readjusted. \\
The following section describes the core design goals and principles of the
middleware \gls{ROS}.

\newpage

\subsection{Design Goals of ROS}
Writing software for robots is difficult, especially as the scale and scope of
robotics continues to grow. \\
\gls{ROS} provides a collection of tools, libraries, and conventions that aim to
simplify the task of creating complex and robust robot behavior across a wide
variety of robotic platforms. \\
\gls{ROS} handles several common use cases of robotics software development and the
philosophical goals of \gls{ROS} can be summarized as: \\
\vspace{-5mm}
\setlist{noitemsep} 
\begin{itemize}
	\item Peer-to-peer
	\item Tools-based
	\item Multilingual
	\item Thin
	\item Free and open-source
\end{itemize}

\noindent A system built using \gls{ROS} consists of numerous small computer programs
that connect to one another and continuously exchange messages without a central
routing service. To establish a peer-to-peer connection every \gls{ROS} system needs a
\gls{ROS} master or roscore, which contains all the relevant information for the
different programs. This architecture increases the scalability of the system.
\\
To gain stability and manage the complexity, \gls{ROS} is based of a large number of
small tools to build and run the various \gls{ROS} components, rather than
constructing a monolithic development and runtime environment. \\
\gls{ROS} chooses a multilingual approach to write modules in the language programmer
wants to or fits the best. This is possible because \gls{ROS} communications following a
convention that describes how messages should be serialized. \\
Furthermore the \gls{ROS} conventions encourage contributors to create standalone
libraries that allow the reuse of software outside of \gls{ROS} for other applications
and simplifies the creation of automated tests. \\
Last but not least, \gls{ROS} is distributed under the terms of the BSD license, which
allows the development of both non-commercial and commercial projects.
\cite{Quig15}

\subsection{Core Components of ROS}
One of the primary purposes of \gls{ROS} is to facilitate communications between the \gls{ROS} nodes. The main mechanism used by \gls{ROS} nodes to communicate is by sending and receiving messages following the publisher-subscriber pattern. \\
The core components of \gls{ROS} are: \\
\vspace{-5mm}
\setlist{noitemsep} 
\begin{itemize}
	\item Nodes
	\item Topics
	\item Messages
	\item Master
\end{itemize}

\noindent \gls{ROS} nodes are processes that independently perform some computation and
interacts with other nodes using the \gls{ROS} communication capability. One of the
strengths of \gls{ROS} is that a particular task, such as developing an autonomous
\gls{RC}-Car, can be separated into a series of simpler tasks. Each of these actions
might consist of a \gls{ROS} node or a series of nodes to accomplish the specific tasks, where a launch file can start multiple nodes at the same time. \\

\noindent A number of independent nodes that comprise a graph, are communicating through topics. A topic is a name for a stream of messages with a defined type. Nodes can publish data to topics or subscribe to topics for using specific messages. Before nodes can transmit data over topics, they must first advertise, both the topic name and the types of messages that are going to be sent. For receiving information on a topic, nodes have to subscribe to it by making a request to the roscore, which is described in more detail below. \\

\noindent A message is a simple data structure, comprising typed fields. This could be standard primitive types like integer, floating points, boolean, etc., but also arrays of primitive types which are especially used for images. Messages can also contain arbitrarily nested structures and arrays. \\

\noindent Since the \gls{ROS} system is based on peer-to-peer, the nodes have to find each other, which is made possible through registration at the \gls{ROS} master. The \gls{ROS} master provides naming and registration services and tracks publishers and subscribers in the \gls{ROS} system. By starting the roscore, the master is getting invoked next to other essential components like the \gls{ROS} parameter server and the rosout logging node.\cite{Quig09} \\
Figure 2.4 visualizes the communication workflow of the core components of a \gls{ROS} system and illustrates the registration of nodes and topics with dashed arrows and the actual information flow through publishing and the usage of callbacks with solid arrows.

\begin{figure}[ht]
	\centering
	\includegraphics[width=9cm]{images/ros_communication.png}
	\caption{Communication in a \gls{ROS} system}
\end{figure}

\subsection{Visualization Tools}
\gls{ROS} follows the Unix philosophy of software development in several key aspects, is based completely on terminals and has no graphical development environments like on Windows or Mac OS X. Therefore \gls{ROS} comes with multiple visualization tools to display information and the workflow in a more human readable representation. \\
The most commonly used tools are the RViz Visualization and the rqt graph. \\
The RViz is an official 3D visualization tool that can display almost any kind of data from sensors and also information within the \gls{ROS} toolchain. In the project study it is especially used to show modified images on different topics. The rqt graph provides a GUI plugin for visualizing the \gls{ROS} computation graph. This gives you a very good overview of the various nodes and which topics they use to communicate with each other.

\section{Smart Automobile Munich}
In the Smart Automobile Munich (\gls{SAM}) project study, students learn to apply work
practices and techniques of project management and situational learning by means
of a concrete, complex project in the field of computer sciences under realistic
conditions. \\
Specifically, an autonomous \gls{RC}-Car has been developed for several years, which
annually participates in competitions such as the Audi Autonomous Driving Cup
(\gls{AADC}) and the Autonomous Driving Challenge of the Verein Deutscher Ingenieure
(\gls{VDI ADC}) against other universities in Germany. \\

\noindent Every semester there are new students participating and implementing
different kinds of features or optimizing existing ones, to increase the
stability and scope of the software system. These includes various
functionalities like parking, object detection, localization and mapping,
general architecture designs and much more. \\

\noindent The following section gives a brief overview of the different concepts
and methods of the \gls{SAM} project study. Next to the hardware platform and software
architecture, the various \gls{ROS} packages, interfaces and project structures are
described in more detail.

\subsection{Scrum and GitLab}
To increase the productivity and efficiency of such a complex project, and the
communication effort between students, some techniques of the project management
method Scrum are used. \\
Scrum is a framework that helps working together, encourages teams to learn
through experiences, self-organize while working on a problem and reflect their
wins and losses to continuously improve.\cite{SCRUM21} \\

\noindent Students are split into different teams with different tasks, and each
team chooses its own scrum master, who is responsible for progress and
collaboration. In addition, there are one or two project managers who are
responsible for the overall project and support communication between the
teams, set intern deadlines and schedule regular meetings. Next to the coaching
and mentoring professors, there are always supportive tutors and student
assistants who have already gained experience in the project and are always open
for questions. \\
Figure 2.5 represents the organization chart with the different feature teams.

\begin{figure}[ht]
	\centering
	\includegraphics[width=14cm]{images/organization_chart_sam_orig.png}
	\caption{Organization chart of the \gls{SAM} project study}
\end{figure}

\noindent The semester is divided into four milestones and the teams have to
present their ideas and progress after the second and fourth one. The tasks and
features have to be defined in GitLab issues and are managed within the issue
tracking board. \\
GitLab is a web-based DevOps lifecycle tool that provides a git repository
manager with a documentation wiki, continuous integration and deployment
pipeline, the mentioned issue-tracking and much more managing and versioning
instruments.\cite{GITLAB} \\

\noindent In addition to weekly meetings with all participants, including
professors and tutors, a regular meeting with project and team leaders is
organized. In this conference, difficulties and problems, software changes and
new conventions are discussed. \\
The information gathered is then passed to the team members via the team
leaders. Furthermore, regular mood surveys and retrospectives are conducted for
reflective evaluation of the work done so far, which are very important
artifacts of the scrum process.

\newpage

\subsection{Platform / Hardware}
At the beginning, Audi not only provided the software framework \gls{ADTF} but also
equipped the participating universities with the same hardware platforms. Since
the \gls{AADC} competition is no longer conducted, there is no hardware restriction
anymore. In the summer semester 2021 the new MX CarKit platform was introduced
and a dedicated team focused on the wiring and preparation for the use of the
platform. \\
Since all features are based on the previously used hardware and the new CarKit
has not yet been completed, this work will focus exclusively on the \gls{AADC}
platform. \\

\noindent The \gls{RC}-Car has the scale of 1:8 and is based on a x86 processor
architecture with a GIGABYTE GA-Z170N-WIFI Intel i3 CPU. Especially for the
object detection and machine learning algorithms, there is a Nvidea GeForce GTX
1050Ti graphic card installed, which provides the necessary computing power. \\
For the detection of the environment, several vehicle sensors were installed,
which are controlled via four arduino microboards. The lidar is located at the
front of the vehicle, centered between the wheels and is currently mainly used
for the emergency brake and the obstacle avoidance. \\
Further up, the Pylon Basler camera is connected and offers a large field of
view thanks to its wide-angle fisheye and serves as input for various autonomous
driving functions such as the parking and also the lane following package. \\
To ensure further safety and to better sense the environment around the vehicle, additional five ultrasonic sensors were mounted on the sides and rear of the \gls{RC}-Car. \\
One of the most important tasks of an autonomous robot is to determine its position in space. For this localization, the \gls{RC}-Car has a wheel sensor and an inertial measurement unit (MSU), which provides information about the driven distance, the actual speed and the current steering angle. \\
The vehicle actuator is controlled by an arduino and consists of a servo for the control, the speed controller and the motor. The motor is powered by a 7.4V LiPo battery, while the computer is powered by a 22.2V LiPo battery. In addition, the \gls{RC}-Car can also be supplied with power via a corresponding power supply unit, which is a great advantage, especially during development. \\
Figure 2.6 shows the \gls{RC}-Car from the front and from a bird's eye
view with all its hardware components.

\newpage

\begin{figure}[ht]
	\centering
	\includegraphics[width=15cm]{images/old_hardware.PNG}
	\caption{\gls{AADC} hardware platform}
\end{figure}

\subsection{Software Design}
In the summer semester of 2020, the underlying framework \gls{ADTF} was replaced by the open-source framework \gls{ROS}. Since then, the developed modules are ported into the newly set up \gls{ROS} system and adapted to the new architecture. In addition, the project is under continuous development, so the architecture design and several functionalities have changed during the work. \\

\noindent The software architecture of the \gls{ROS} project study, follows a basic Evaluate-Plan-Act Pattern where the evaluation and the planning is done for every dedicated \gls{ROS} package on its own. Figure 2.7 shows the currently architecture with all ported modules and the overall workflow of the \gls{RC}-Car. \\
On the left side of the illustration are all the sensor described in section 2.3.2 that are mounted on the \gls{RC}-Car. In addition, another Realsense Camera is used, which is temporarily attached to the vehicle, contains additional information about depth and tracking, and is mainly used with ne new architecture. \\
In the middle area of the graphic are all \gls{ROS} packages of the software system, which communicate with each other and make up the actual core of the software architecture. The process and the \gls{ROS} packages are divided into three steps, which are color-coded to give a certain clarity. \\

\begin{sidewaysfigure}[ht]
	\centering
	\includegraphics[width=20cm]{images/overall_project_architecture.png}
	\caption{Complete Project Architecture}
\end{sidewaysfigure}

The blue rectangles summarize all modules responsible for the perception of the environment. These include packages that record, configure and also process the sensor data, such as object recognition and line segment detection. \\
The prepared and processed data is then passed on to the planning phase, marked as orange rectangles. The modules mainly include functionalities that create trajectories or plan several next possible steps, which are then passed on to the decision phase. The lane follower, the parking module and the emergency brake, for example, fall into this category. \\
The decision phase comes last and decides which next steps to take, selects the route to be driven and finally executes it by sending the desired speed and the corresponding steering angle to the actuators. For priority-based selection, speed (float), steering angle (float) and trajectory multiplexers are used, which are partly redundant in the current development phase and have not yet been optimized. So far, there are different ways to send the speed and the steering angle to the car controller, which affects clarity and performance. The float multiplexer, the trajectory multiplexer and the workflow around them will be looked at again in more detail in the analysis of the lane follower in a later chapter. \\
In addition, utility modules such as visualization, trajectory testing or the external control of various planning and execution packages are shown in pink rectangles. \\
Finally, the visualization shows the connection to the simulation software Carla, which runs on its own server and is connected to the \gls{ROS} system via a bridge. The simulation is intended to facilitate development without hardware, although this functionality is still in an early stage of development and cannot yet be used for this work. \\
The graphic is a first attempt to visualize all \gls{ROS} packages and the workflow of the software. In the next semesters it has to be further revised, adapted and improved. \\

\noindent The project directory structure can be seen in figure 2.6 and is described in more detail below. It gives a brief overview of the core components of the software and what it needs to build and run the complete project. \\

\begin{figure}[ht]
	\centering
	\includegraphics[width=3cm]{images/project_structure.png}
	\caption{The Project Structure in directory view}
\end{figure}

\noindent The software uses Docker for more security and further encapsulation of tasks. After the architecture change in the summer semester of 2021, where each \gls{ROS} package was in its own docker container, the current design has only four containers, which are started separately or together. There is a container for the development, where no hardware nodes like the lidar or car controller is started, a hardware container that starts all the standard packages to run the software on the \gls{RC}-Car, a simulation container for creating the simulation environment and an object detection container, because it has a lot of own dependencies and used libraries that should not be combined with all the dependencies of the other packages. The docker images and the according entrypoints are stored in the docker folder. \\
To start respective images together or with specific configurations, the build automation tool make is used that automatically builds executable programs and libraries from source code by reading the Makefile, which is located at the root directory of the project. \\
Once the docker images are running the catkin workspace is initialized and a terminal opened that is connected to it, there are multiple \gls{ROS} launch files that starts various combinations of the nodes to start the software. Launch files for the hardware, the develop environment, testing and visualizations are stored in the launch\_files folder. The catkin\_ws folder in the root directory of the project is a persistent folder, so you do not need to build the whole software over and over again, when u restart the docker containers. \\

\noindent The ros-packages folder includes all \gls{ROS} packages and the implementation of the nodes that are linked within the entrypoint scripts into the docker environments. \\ 
The config folder contains some configuration files that are needed for initializing some basic parameters, which multiple packages and nodes needs and the settings for calibrating and undistorting the fisheye image. The rosmaster\_shared folder is also linked into the docker container and contains some shared scripts and recorded rosbag files with specific scenarios for testing. \\

\noindent The .gitlab folder and the .gitlab-ci.yml file are configurations and setting for the used web-based DevOps lifecycle tool Gitlab. The .gitlab folder contains some templates for feature, bug or ideas issues but also the convention for the merge request. \\
the .yml file defines configurations and jobs for the continuous integration and delivery on the GitLab platform. It builds the complete project and checks if everything works as it should and runs all the tests to verify the functionality of the features.