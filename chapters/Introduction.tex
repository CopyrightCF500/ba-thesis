% !TEX root = ../main.tex
\chapter{Introduction}
\label{chapter:Introduction}

\section{Thesis Motives}
The first chapter will give a brief overview, why the thematic of analyzing and optimizing a ROS-based lane follower was chosen, what's the current status within the project study where a self-driving \gls{RC}-Car is developed and what problems currently occurring.\\
Furthermore, a general overview and contribution about the procedure of this work is described.
\subsection{Motivation}
It is not only the mobility that is experiencing a global transformation. The autonomous car is also affected by other megatrends related to mobility, especially connectivity, urbanization, sustainability, electrification and sharing.\cite{Her18} \\
In addition to the 1.25 million annual traffic deaths and the 50 million traffic injuries \cite{WHO18}, there are many other reasons for autonomous technologies. \\
The population growth in the large cities combined with the growing demand for individual mobility is leading to increased emissions and significantly higher numbers of accidents. \\
Technology can provide people with the desired mobility more efficiently and in a way that is more resource-friendly than human drivers. Information technology will no longer be added to the car, but that car will be build around the information technology. \cite{Her18} \\

\noindent At the University of Applied Sciences in Munich, an autonomous \gls{RC}-Car is being developed by students in a project study. \\
In the winter semester of 2019, the middleware Robot Operating System (\gls{ROS})\cite{ROS_WIKI} was set up as a stand-alone project in addition to the previously used Automotive Data and Time-Triggered Framework (\gls{ADTF})\cite{ADTF_WIKI}. \\
For this reason, many already implemented functionalities and modules from the \gls{ADTF} project are currently being ported to the ROS project, resulting in many interesting topics.\\
One of the most crucial tasks in autonomous driving is the lane-detection and lane-keeping assistant. \\

\noindent During my bachelor thesis in summer semester 2021, I participated in the Smart Automobile Munich (\gls{SAM}) project study on a voluntary basis in addition to my bachelor thesis, in order to get a better understanding of the whole project and for collaborating with my fellow students. \\
The various tasks of developing the self-driving \gls{RC}-Car are handled by different teams, following the Scrum process model. In addition to the role as project manager, I participated in team 6, which was responsible for refactoring the docker architecture, continuous integration and merging the feature branches of the teams. \\

\begin{figure}[ht]
	\centering
	\includegraphics[width=13cm]{images/project_room.png}
	\caption{Project room with test track, cars and monitors}
\end{figure}

\subsection{State of the art}
The lane follower was initially implemented in 2017 within the \gls{ADTF} framework and has performed very well. During the migration to the \gls{ROS} framework, the core functionalities of the lane follower were ported to the \gls{ROS} project. \\
It receives as input the calibrated and undistorted camera image, calculates the corresponding steering angle and passes it to the car controller. A more detailed description of the data flow will follow in section 3.1.1. At this time, trajectories with steering angle and speed information for multiple points of the driving path, were not yet implemented and the velocity is calculated independently of the road layout.
Through this first porting, many open features need to be expanded or redesigned.

\subsection{Problem Statement}
After starting the software and running the lane follower, the \gls{RC}-Car detects and follows the lanes and it can drive most of the times several circles on the test track. \\
But there are a lot of different problems and deprecated dependencies within the lane follower, and also in the whole system, which indirectly affect the lane follower. \\

\noindent The most obvious and main problem can be seen when the \gls{RC}-Car follows the track. Besides very low and not constant speed, the car does not follow the calculated center line perfectly but changes the steering angle sometimes very abruptly and drives in small serpentines. \\ 
This behavior is probably due to the fact that the lane follower detects the position of the centerline to be driven off at the current time and does not provide a prediction about steering angle and speed in the future. \\

\noindent In addition to the problems with the functionality and execution itself, there are other optimization needs. 
Rapid porting from \gls{ADTF} to \gls{ROS} transferred outdated architecture designs, created redundant functionality, carried unnecessary and obsolete dependencies, and left documentation unrevised.
These and other issues will be analyzed and addressed in more detail in the thesis.

\section{Contribution and Overview}
This thesis is mainly concerned with the analysis and development of optimization approaches for the lane follower and its interfaces such as the line segment detection, usage of trajectories and deprecated dependencies.\\
In addition, a first evaluation method for the performance of the lane follower is created, which can be used to measure changes in the future. \\

\noindent In the first part of the thesis the theoretical background is explained and different preconditions for the functionality and classification of the lane follower are described. \\
This includes a general overview about the real word autonomous driving assistant systems, the different levels of autonomy, definitions of lane following and related work, and also the description of the usual workflow from the sensors to the actuators.\\
After that, the focus is more about the \gls{RC}-Car within the project study, like the Robot Operating System. Basic concepts, benefits and opportunities through the usage of \gls{ROS}, and module communication are described here. \\
The lane follower is a core functionality of a complex software system, which is created by students within the Smart Automobile Munich lecture. Therefore the next section is about the project study with the used Scrum workflow, the hardware platform and the general software architecture with its structure patterns. \\

\noindent Chapter 3 analyses the actual lane follower workflow and all the related \gls{ROS} packages that are needed or will be after the optimizations and extensions. This includes the Speed and Steering Angle Multiplexer, the Remote Controller, the Trajectory Multiplexer, the Line Segment Detection and the Traffic Sign Detection. \\
These modules are defined packages in the \gls{ROS} system, whereas in the subsequent section the package structure of the lane follower itself is examined in more detail with its architecture design, general algorithm pipeline and all the used modules and headers. \\
Finally, this chapter identifies problems and options for improvement, which are then addressed and implemented in the following part. \\

\noindent Chapter four describes the development preparations and implements the different methods and optimizations. This includes especially the outsourcing of the road detection and integration of the Line Segment Detection Node and the usage of the Traffic Sign Recognition for regulating the speed within the lane follower accordingly to the road layout. \\
Additionally, will no longer only sends the computed steering angle, but creates a trajectory with speed and turning angle as information. \\
Furthermore, the lane follower consists of a lot of internal modules that were needed in the previous \gls{ADTF} system and uses obsolete dependencies which will be removed so the lane follower and the overall system gets less complex and will be simplified. \\
Subsequently, the performance and behavior of the \gls{RC}-Car is evaluated with self-developed evaluation metrics and compared with the previous state. \\

\noindent Finally, the results are presented and a conclusion is formed. Next to the considered and implemented optimizations, there are a lot of other ideas and functionalities that could be implemented and improved. Possible future work is described in section 5.2. \\
Summarized, the following approaches are analyzed, and optimizations contributed: 
\begin{itemize}
	\item Analysis of Lane Follower
	\begin{itemize}
		\item Lane Follower Node
		\item Related \gls{ROS} Nodes
		\item Lane Follower Package Structure
		\item Identified Issues
	\end{itemize}
	\item Optimizations and Extensions
	\begin{itemize}
		\item Integration of Line Segment Detection
		\item Speed Regulation
		\item Usage of Trajectories
		\item Removing deprecated dependencies and Dead Code
	\end{itemize}
	\item Evaluation Methods
	
\end{itemize}